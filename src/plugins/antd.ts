import {
    Button,
    Form,
    Input,
    Checkbox,
    Row,
    Col,
    Menu,
    ConfigProvider,
    Layout,
    Breadcrumb,
    Calendar,
    Radio,
    Upload,
    Table,
    Divider,
    Descriptions,
    Modal,
    Avatar,
    Select
  } from 'ant-design-vue'
  /**
   * @description 手动注册 antd-vue 组件,达到按需加载目的
   * @description Automatically register components under Button, such as Button.Group
   * @param {ReturnType<typeof createApp>} app 整个应用的实例
   * @returns void
   */
  export default function loadComponent(app: any) {
    app.use(Button)
    app.use(Form)
    app.use(Input)
    app.use(Checkbox)
    app.use(Row)
    app.use(Col)
    app.use(Menu)
    app.use(ConfigProvider)
    app.use(Layout)
    app.use(Breadcrumb)
    app.use(Calendar)
    app.use(Radio)
    app.use(Upload)
    app.use(Table)
    app.use(Divider)
    app.use(Descriptions)
    app.use(Modal)
    app.use(Avatar)
    app.use(Select)
  }
  