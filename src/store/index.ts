import { createStore } from "vuex";
import {GetUserInfo} from "@/api/api"
import zh_CN from "ant-design-vue/lib/locale-provider/zh_CN";
import en from "ant-design-vue/lib/locale-provider/en_US";
type languageType = "en" | "zh_CN";
export default createStore({
  state: {
    userInfo: {},
    locale: "zh_CN",
    languageAtd: zh_CN,
    islogin: false,
  },
  mutations: {
    //切换语言
    CHANGE_LANGUAGE(state, data: languageType) {
      state.locale = data;
      console.log(data, 11);
      if (data == "zh_CN") {
        state.languageAtd = zh_CN as any;
      } else if (data == "en") {
        state.languageAtd = en as any;
      }
    },
    SAVE_USERINFO(state, data: any) {
      //登陆
      state.userInfo = data;
      state.islogin = true;
      
    },
    UPDATE_USERINFO(state, data:any ){
      //修改用户信息
      state.userInfo = {};
      state.userInfo = data;
      console.log('修改成功！',state)
    },
    LOGOUT(state) {
      //登出
      state.islogin = false;
      state.userInfo = {};
      sessionStorage.removeItem('userInfo');
      sessionStorage.removeItem('accessToken');
    },
  },
  actions: {
    getNewUserInfo(context,id:Number){ 
      GetUserInfo(id).then((res:any)=>{
        console.log(res)
        if(res.code == 0){
          context.commit('UPDATE_USERINFO',res.data)
          sessionStorage.setItem(
            "userInfo",
            JSON.stringify(res.data)
          );
        }
      }).catch((err)=>{
        console.log(err)
      })
    }
  },
  modules: {},
});
