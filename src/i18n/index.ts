import {useI18n} from 'vue-composable'
import en from './en'
import zh_CN from './zh_CN'
import store from '@/store/index'
import {computed } from 'vue'
//let  currentLanguage = store.state.locale
const currentLanguage: any = computed(() => store.state.locale);
export const i18n = useI18n({
    locale: currentLanguage as any,
    messages: {
      en, 
      zh_CN
    }
  });