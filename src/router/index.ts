import { Breadcrumb } from "ant-design-vue";
import {
  createRouter,
  createWebHistory,
  RouteRecordRaw,
  createWebHashHistory,
} from "vue-router";
import store from "../store/index";

var routes: Array<RouteRecordRaw> = [
  {
    path: "",
    redirect: "/login",
  },
  {
    path: "/login",
    name: "Login",
    meta: {
      roles: ["ADMIN", "USER"],
    },
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/main",
    name: "Main",
    redirect: "/main/homepage",
    component: () => import("../layout/index.vue"),
    children: [
      /* {
        path:'/main',
        redirect:'/main/homepage'
      }, */
      {
        path: "/main/homepage",
        name: "Homepage",
        meta: {
          roles: ["ADMIN", "USER"],
          breadBar: [
            {
              name: "首页",
            },
          ],
          breadBarEn: [
            {
              name: "HomePage",
            },
          ],
        },
        component: () => import("../views/Homepage.vue"),
      },
      {
        path: "/main/upload",
        name: "Upload",
        meta: {
          roles: ["ADMIN", "USER"],
          breadBar: [
            {
              name: "上传",
            },
          ],
          breadBarEn: [
            {
              name: "Video Upload",
            },
          ],
        },
        component: () => import("../views/Upload.vue"),
      },
      {
        path: "/main/result/:id",
        name: "Result",
        meta: {
          roles: ["ADMIN", "USER"],

          breadBar: [
            {
              name: "历史纪录",
            },
            {
              name: "详情",
            },
          ],
          breadBarEn: [
            {
              name: "History",
            },
            {
              name: "Result details",
            },
          ],
        },
        component: () => import("../views/Result.vue"),
      },
      {
        path: "/main/history",
        name: "History",
        meta: {
          roles: ["ADMIN", "USER"],
          breadBar: [
            {
              name: "历史纪录",
            },
          ],
          breadBarEn: [
            {
              name: "History",
            },
          ],
        },
        component: () => import("../views/History.vue"),
      },
      {
        path: "/main/setting",
        name: "Setting",
        meta: {
          roles: ["ADMIN", "USER"],
          breadBar: [
            {
              name: "设置",
            },
          ],
          breadBarEn: [
            {
              name: "Setting",
            },
          ],
        },
        component: () => import("../views/Setting.vue"),
      },
      {
        path: "/main/usermanage",
        name: "UserManage",
        meta: {
          roles: ["ADMIN"],
          breadBar: [
            {
              name: "用户管理",
            },
          ],
          breadBarEn: [
            {
              name: "UserManage",
            },
          ],
        },
        component: () => import("../views/Usermanage.vue"),
      },
      {
        path: "/main/cardmanage",
        name: "CardManage",
        meta: {
          roles: ["ADMIN"],
          breadBar: [
            {
              name: "精康检测卡管理",
            },
          ],
          breadBarEn: [
            {
              name: "CardManage",
            },
          ],
        },
        component: () => import("../views/Cardmanage.vue"),
      },
      {
        path: "/main/404",
        name: "404",
        meta: {
          roles: ["ADMIN", "USER"],
          breadBar: [
            {
              name: "错误页面",
            },
          ],
        },
        component: () => import("../views/404.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
});
router.beforeEach((to, from) => {
  const userInfo: any = store.state.userInfo;
  /* console.log(to,'to')
  console.log(store.state.userInfo,'state') */
  if (to.path == "/login") {
    if (!store.state.islogin) {
      return true;
    } else {
      console.log("拦住了");
      return false;
    }
  } else if (to.meta.roles.includes(userInfo.userType)) {
    return true;
  } else {
    return "/main/404";
  }
});
export default router;
