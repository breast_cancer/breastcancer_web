export default {
  renderTime(date: string) {
    //格林尼治时间转为
    var dateee = new Date(date).toJSON();
    return new Date(+new Date(dateee) + 8 * 3600 * 1000)
      .toISOString()
      .replace(/T/g, " ")
      .replace(/\.[\d]{3}Z/, "");
  },
  getUuid() {
    let date = new Date();
    let Y = date.getFullYear() + "-";
    let M =
      date.getMonth() + 1 < 10
        ? "0" + (date.getMonth() + 1)
        : date.getMonth() + 1;
    let D = (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + "-";
    let Ymd = Y + M + D;
    const random = "xxxx-4xxx-yxxx-xxxx".replace(/[xy]/g, (c) => {
      var r = (Math.random() * 16) | 0,
        v = c == "x" ? r : (r & 0x3) | 0x8;

      return v.toString(16);
    });
    return Ymd + random;
  },
};
