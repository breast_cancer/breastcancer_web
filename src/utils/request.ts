import axios from "axios";
let baseURL: string = "http://39.105.98.101:8899";  //http://39.105.98.101:8899 http://localhost:8899
const service = axios.create({
  baseURL,
  timeout: 30000,
});
service.interceptors.request.use(
  (config: any) => {
    //console.log("拦截了");
    /* 
      config.headers.common.Authorization = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MDc2NTk1NTIsImV4cCI6MTYwNzY1OTYxMiwidXNlcklkIjoiMTIzIn0.gvRf9k1QFxqboxymSRl4AkxmJDrI8FwIRB4ljf412Ig';
      config.headers['Refresh-Token']='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MDc2NTk1NTIsImV4cCI6MTYwNzY1OTY3MiwidXNlcklkIjoiMTIzIn0.rDZTdYFJYNTBKfcbHSnmm0zXcoUuojQyeDJWi3Ne1_U' */
    // 如果有token 就携带tokon
    let token = sessionStorage.getItem("accessToken");
    if (token) {
      //console.log(token, "token");
      config.headers.common.Authorization = token;
    }
    return config;
  },
  (error: any) => Promise.reject(error)
);
// 响应拦截器
service.interceptors.response.use(
  (response) => {
    if (response.data.code == 0) {
      if (response.data.data.token) {
        sessionStorage.setItem("accessToken", response.data.data.token);
      }
    }
    return response;
  },
  (error: any) => {
    console.log(error + "err1111"); // for debug
    return Promise.reject(error);
  }
);
export default {
  get(url: string, params?: any) {
    return new Promise((resolve, reject) => {
      service
        .get(url, {
          params: params,
        })
        .then((res: any) => {
          resolve(res.data);
        })
        .catch((err: any) => {
          reject(err.data);
        });
    });
  },
  post(url: string, params: any) {
    return new Promise((resolve, reject) => {
      service
        .post(url, params)
        .then((res: any) => {
          resolve(res.data);
        })
        .catch((err: any) => {
          reject(err.data);
        });
    });
  },
  put(url: string, params: any) {
    return new Promise((resolve, reject) => {
      service
        .put(url, params)
        .then((res: any) => {
          resolve(res.data);
        })
        .catch((err: any) => {
          reject(err.data);
        });
    });
  },
  baseURL
};
