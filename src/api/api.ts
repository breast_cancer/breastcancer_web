import api from "../utils/request";
export const login = (params: any) => {
  //登陆
  return api.post("/sysUser/login", params);
};
export const JkcardInfo = (id: Number) => {
  //获取该id所绑定的精康检测卡信息
  return api.get(`/sysIcCard/checkIcCardByUserId/${id}`);
};
export const UpdateUserInfo = (params: any) => {
  //修改/新增 用户信息
  return api.post("/sysUser/registerOrUpdate", params);
};
export const GetUserInfo = (id: Number) => {
  //获取用户详细信息
  return api.get(`/sysUser/${id}`);
};
export const GetAllUsers = (params: any) => {
  //获取所有用户
  return api.get(
    `/sysUser/getAllUserList?current=${params.current}&size=${params.size}`
  );
};
export const AddOrEditNewCard = (params: any) => {
  //精康检测卡注册或修改
  return api.post("/sysIcCard/saveOrUpdate", params);
};
export const getAllCards = (params: any) => {
  //获取所有精康检测卡
  return api.get(
    `/sysIcCard/getAllIcList?current=${params.current}&size=${params.size}`
  );
};
export const bindCard = (params: any) => {
  //绑定精康检测卡
  return api.put("/sysIcCard/userBindIcCard", params);
};
export const goToCalculate = (params: any) => {
  //计算
  return api.post("/sysResult", params);
};
export const getResultById = (Id:any) => {
    //根据id获取历史纪录详情
    return api.get(`/sysResult/${Id}`)
};
export const getAllResults = (params:any) => {
    //根据条件查询历史纪录
    return api.get(`/sysResult/getResult`,params)
};
export const reduceIcNumber = (params:any) =>{
  //计算成功后精康检测卡次数-1
  return api.put(`/sysIcCard/reduceIcNumber`,params)
};
export const deletResult = (params:any) => {
  //删除计算结果
  return api.put('/sysResult/deleteResultById',params)
}
export const deleteUser = (params:any) => {
  //删除用户
  return api.put('/sysUser',params)
}
export const downloadRes = (params:any) => {
  //下载
  return api.get(`/download/downloadHistory/${params}`)
}
export const addDoctorNotes = (params:any) => {
  //提交医生意见
  return api.put("/sysResult", params)
}
